package goLinker_test

import (
	"russ/goLinker"
	"testing"

	"golang.org/x/exp/maps"
	"golang.org/x/exp/slices"
)

type testIntStruct struct {
	Num         int
	StringValue string
}

type testFloatStruct struct {
	Num         float64
	StringValue string
}

type testMergeStruct struct {
	Num1         int
	StringValue1 string
	Num2         float64
	StringValue2 string
}

func Test_Sum(t *testing.T) {
	//Arrange
	arrays := []testIntStruct{
		{Num: 2, StringValue: "test"},
		{Num: 3, StringValue: "test2"},
	}

	//Act
	getter :=
		func(input testIntStruct) int { return input.Num }
	result := goLinker.Sum(arrays, getter)

	//Assert
	if result != 5 {
		t.Fail()
	}
}

func Test_Avg(t *testing.T) {
	//Arrange
	arrays := []testFloatStruct{
		{Num: 2, StringValue: "test"},
		{Num: 3, StringValue: "test2"},
	}

	//Act
	getter :=
		func(input testFloatStruct) float64 { return input.Num }
	result := goLinker.Avg(arrays, getter)

	//Assert
	if result != 2.5 {
		t.Fail()
	}
}

func Test_ToMap(t *testing.T) {
	//Arrange
	arrays := []testIntStruct{
		{Num: 1, StringValue: "test1"},
		{Num: 2, StringValue: "test2_1"},
		{Num: 2, StringValue: "test2_2"},
		{Num: 2, StringValue: "test2_3"},
		{Num: 3, StringValue: "test3_1"},
		{Num: 3, StringValue: "test3_2"},
	}
	expected := map[int][]testIntStruct{
		1: {{Num: 1, StringValue: "test1"}},
		2: {{Num: 2, StringValue: "test2_1"},
			{Num: 2, StringValue: "test2_2"},
			{Num: 2, StringValue: "test2_3"}},
		3: {{Num: 3, StringValue: "test3_1"},
			{Num: 3, StringValue: "test3_2"}},
	}

	//Act
	getKey :=
		func(input testIntStruct) int { return input.Num }
	result := goLinker.ToMap(arrays, getKey)

	//Assert
	equal :=
		func(v1 []testIntStruct, v2 []testIntStruct) bool { return slices.Equal(v1, v2) }
	if !maps.EqualFunc(expected, result, equal) {
		t.Fail()
	}
}

func Test_ToSlice(t *testing.T) {
	//Arrange
	inputMap := map[int][]testIntStruct{
		1: {{Num: 1, StringValue: "test1"}},
		2: {{Num: 2, StringValue: "test2_1"},
			{Num: 2, StringValue: "test2_2"},
			{Num: 2, StringValue: "test2_3"}},
		3: {{Num: 3, StringValue: "test3_1"},
			{Num: 3, StringValue: "test3_2"}},
	}
	expected := []testIntStruct{
		{Num: 1, StringValue: "test1"},
		{Num: 2, StringValue: "test2_1"},
		{Num: 2, StringValue: "test2_2"},
		{Num: 2, StringValue: "test2_3"},
		{Num: 3, StringValue: "test3_1"},
		{Num: 3, StringValue: "test3_2"},
	}

	//Act
	result := goLinker.ToSlice(inputMap)

	//Assert
	if !slices.Equal(result, expected) {
		t.Fail()
	}
}

func Test_Merge(t *testing.T) {
	//Arrange
	rightArrays := []testIntStruct{
		{Num: 1, StringValue: "right1"},
		{Num: 2, StringValue: "right2"},
		{Num: 3, StringValue: "right3"},
		{Num: 4, StringValue: "right4"},
		{Num: 5, StringValue: "right5"},
		{Num: 6, StringValue: "right6"},
	}
	leftArrays := []testFloatStruct{
		{Num: 1, StringValue: "left1"},
		{Num: 2, StringValue: "left2"},
		{Num: 3, StringValue: "left3"},
	}
	expected := []testMergeStruct{
		{Num1: 1, StringValue1: "right1", Num2: 1, StringValue2: "left1"},
		{Num1: 2, StringValue1: "right2", Num2: 2, StringValue2: "left2"},
		{Num1: 3, StringValue1: "right3", Num2: 3, StringValue2: "left3"},
	}

	//Act
	equals :=
		func(right testIntStruct, left testFloatStruct) bool {
			return float64(right.Num) == left.Num
		}
	merge :=
		func(right testIntStruct, left testFloatStruct) *testMergeStruct {
			return &testMergeStruct{
				Num1:         right.Num,
				StringValue1: right.StringValue,
				Num2:         left.Num,
				StringValue2: left.StringValue,
			}
		}
	result := goLinker.Merge(rightArrays, leftArrays, equals, merge)

	//Assert
	if !slices.Equal(result, expected) {
		t.Fail()
	}
}
